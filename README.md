
# Pheonix UI Automation Framework

This is a test project for testing a few scenarios on a CRM website(Pheonix). There are 5 tests in total spread over 5 test classes. Underlying, it uses Selenium - open-source tool for web browsers automation, TestNG - Third-party free library for Running tests, and Extent Reports (library for interactive and detailed reports for tests). On top of this, I have implemented Page  and Fluent design patterns. These techniques eliminates the code repetition and increases the readability of automated UI tests and displays only business logic in tests, not displays low-level page elements and selenium API details in tests. Fluent Interface makes tests like natural language and allows anyone to read and understand what the test does. This framework can be used to write UI automation tests for any web application.
- Java 1.8
- Maven
- TestNG
- Selenium


## Features

- Performs UI Automated Tests for Inwarranty Flow.
- Integrated with Jenkins CI for Continious Testing for UI Component.
- Categorizing the Tests in TestNG.
- Performs Parallel Testing and Isolated Tests.


## Tech

- Selenium WebDriver
- WebDriver Manager
- TestNG
- Apache POI
- Log4j
- Extent Report

## Adding New Dependencies

New Dependencies can be added in pom.xml at root level under dependencies tag

Example:


   
		<dependency>
		    <groupId>org.apache.logging.log4j</groupId>
		    <artifactId>log4j-core</artifactId>
		    <version>2.23.1</version>
		</dependency>





## Plugins
Maven Plugins used in the Project
- Maven SureFire Plugins
- Maven compiler Plugin

## To Run the Automated Test for TestNG from cli...
mvn test