package com.ui.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ui.pages.LoginPage;

public class TestLogout {
	private WebDriver wd;
	private LoginPage loginpage;
	@BeforeMethod
	public void setup() {
		wd=new ChromeDriver();
		loginpage=new LoginPage(wd);
		
	}
	@Test
	public void testLogout() {
		Assert.assertEquals(loginpage.doLogin("iamfd", "password").signout().getPageTitile(),"Phoenix - We Repair");
	}
	

}
