package com.ui.tests;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ui.pages.LoginPage;
import com.ui.pojo.CreateJobPOJO;
import com.ui.pojo.SearchJobOrImeiPOJO;
import com.utility.TestUtility;

public class TestSearchJobByID {
	private WebDriver wd;
	private LoginPage loginpage;
	private CreateJobPOJO createjobpojo;
	private String createdjobID;
	@BeforeMethod(description="Setup the browser and creates job")
	public void setup() {
		wd=new ChromeDriver();
		loginpage=new LoginPage(wd);
		createjobpojo=TestUtility.CreateFakeDataForCreateJob();
		String msg=loginpage.doLogin("iamfd", "password").gotoCreateJobPage().createjob(createjobpojo);
		String[] words=msg.split("\\s+");
		createdjobID=words[0];
		System.out.println(createdjobID);
		
	}
	@Test(description="checks if we can search for job by ID",groups= {"smoke,sanity,regression"})
	public void testSearchJob() {
		loginpage=new LoginPage(wd);
		List<SearchJobOrImeiPOJO> jobdetails=loginpage.doLogin("iamfd", "password").getJobOrImeiDetailsFromSearchJobOrImei(createdjobID);
		SearchJobOrImeiPOJO pojo=jobdetails.get(0);
		System.out.println(pojo.getJob_Number());
		Assert.assertEquals(createdjobID,pojo.getJob_Number() );
	}
	

}
