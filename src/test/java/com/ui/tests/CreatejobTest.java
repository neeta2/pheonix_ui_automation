package com.ui.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ui.pages.CreateJobPage;
import com.ui.pages.LoginPage;
import com.ui.pojo.CreateJobPOJO;
import com.utility.TestUtility;

public class CreatejobTest {
	private WebDriver wd;
	private LoginPage loginpage;
	private CreateJobPOJO createjobpojo;
	@BeforeMethod(description="Setup the browser and load the page")
	public void setup() {
		wd=new ChromeDriver();
		loginpage=new LoginPage(wd);
		createjobpojo=TestUtility.CreateFakeDataForCreateJob();
		
	}
	@Test(groups= {"sanity,smoke,regression"})
	public void testcreatejob() {
		String msg=loginpage.doLogin("iamfd", "password").gotoCreateJobPage().createjob(createjobpojo);
		System.out.println(msg);
		
		Assert.assertEquals(msg.contains("Job created successfully"), true);
	}
	

}
