package com.ui.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ui.pages.LoginPage;

public class LoginTest {
	private WebDriver wd;
	private LoginPage page;
	@BeforeMethod
	public void setup() {
		wd=new ChromeDriver();
		page=new LoginPage(wd);
		
	}
	@Test
    public void testLogin() {
		Assert.assertEquals(page.doLogin("iamfd","password").getUserName(),"iamfd");
    	
    }
	
}
