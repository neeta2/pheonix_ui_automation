package com.ui.pojo;

public class CreateJobPOJO {
	private String oemname;
	private String productname;
	private String modelname;
	private String imeinumber;
	private String dateofpurchase;
	private String warrentystatus;
	private String problem;
	private String remarks;
	private String firstname;
	private String lastname;
	private String contact;
	private String email;
	private String flatnum;
	private String apartmentname;
	private String landmark;
	private String streetname;
	private String area;
	private String state;
	private String pincode;
	
	public CreateJobPOJO(String oemname, String productname, String modelname, String imeinumber, String dateofpurchase,
			String warrentystatus, String problem, String remarks, String firstname, String lastname, String contact,
			String email, String flatnum, String apartmentname, String landmark, String streetname, String area,
			String state, String pincode) {
		super();
		this.oemname = oemname;
		this.productname = productname;
		this.modelname = modelname;
		this.imeinumber = imeinumber;
		this.dateofpurchase = dateofpurchase;
		this.warrentystatus = warrentystatus;
		this.problem = problem;
		this.remarks = remarks;
		this.firstname = firstname;
		this.lastname = lastname;
		this.contact = contact;
		this.email = email;
		this.flatnum = flatnum;
		this.apartmentname = apartmentname;
		this.landmark = landmark;
		this.streetname = streetname;
		this.area = area;
		this.state = state;
		this.pincode = pincode;
	}



	







	










	public String getOemname() {
		return oemname;
	}
	public void setOemname(String oemname) {
		this.oemname = oemname;
	}
	public String getProductname() {
		return productname;
	}
	public void setProductname(String productname) {
		this.productname = productname;
	}
	public String getModelname() {
		return modelname;
	}
	public void setModelname(String modelname) {
		this.modelname = modelname;
	}
	public String getImeinumber() {
		return imeinumber;
	}
	public void setImeinumber(String imeinumber) {
		this.imeinumber = imeinumber;
	}
	public String getDateofpurchase() {
		return dateofpurchase;
	}
	public void setDateofpurchase(String dateofpurchase) {
		this.dateofpurchase = dateofpurchase;
	}
	public String getWarrentystatus() {
		return warrentystatus;
	}
	public void setWarrentystatus(String warrentystatus) {
		this.warrentystatus = warrentystatus;
	}
	public String getProblem() {
		return problem;
	}
	public void setProblem(String problem) {
		this.problem = problem;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFlatnum() {
		return flatnum;
	}
	public void setFlatnum(String flatnum) {
		this.flatnum = flatnum;
	}
	public String getApartmentname() {
		return apartmentname;
	}
	public void setApartmentname(String apartmentname) {
		this.apartmentname = apartmentname;
	}
	public String getLandmark() {
		return landmark;
	}
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
	public String getStreetname() {
		return streetname;
	}
	public void setStreetname(String streetname) {
		this.streetname = streetname;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	


	
	

}
