package com.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.utility.BrowserUtil;

public class LoginPage extends BrowserUtil{
	private WebDriver wd;
	private static final By  USER_NAME_TEXTBOX_LOCATOR=By.id("username");
	private static final By PASSWORD_TEXTBOX_LOCATOR=By.id("password");
	private static final By SIGN_BUTTON_LOCATOR=By.xpath("//span[contains(text(),\"Sign in\")]/../..");
	
 public LoginPage(WebDriver wd) {
		super(wd);
		this.wd=wd;
		goToWebsite("http://phoenix.testautomationacademy.in/sign-in");
		 viewinFullScreen();
		// TODO Auto-generated constructor stub
	}

 public DashBoardPage doLogin(String username,String password) {
	 enterText(USER_NAME_TEXTBOX_LOCATOR,"iamfd");
	 enterText(PASSWORD_TEXTBOX_LOCATOR,"password");
	 clickon(SIGN_BUTTON_LOCATOR);
	 DashBoardPage dashboardpage=new DashBoardPage(wd);
	 return dashboardpage;
 }

	

}
