package com.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.ui.pojo.CreateJobPOJO;
import com.utility.BrowserUtil;

public class CreateJobPage extends BrowserUtil{
	private WebDriver wd;
	private static final By OEM_LOCATOR=By.xpath("//mat-select[@placeholder=\"Select OEM\"]/..");
	private static final By PRODUCT_NAME_DROPDOWN_LOCATOR = By
			.xpath("//span[contains(text(), \"Select Product name\")]/../../../..");
	private static final By MODEL_NAME_DROPDOWN_LOCATOR = By
			.xpath("//mat-select[contains(@placeholder,'Select Model name')]");
	private static final By IMEI_TEXT_BOX_LOCATOR = By.xpath("//input[contains(@data-placeholder,'0123456789')]");
	private static final By PURCHASE_DATE_BOX_LOCATOR = By.xpath("//input[@data-placeholder='dd/mm/yyyy']");
	private static final By WARRANTY_DROPDOWN_LOCATOR = By.xpath("//mat-select[@placeholder='Select Warranty Status']");
	private static final By PROBLEM_DROPDOWN_LOCATOR = By.xpath("//mat-select[@placeholder='Select Problem']");
	private static final By REMARK_TEXT_BOX_LOCATOR = By.xpath("//input[@placeholder='Remarks']");
	private static final By FIRST_NAME_TEXT_BOX_LOCATOR = By.xpath("//input[@data-placeholder='First Name']");
	private static final By LAST_NAME_TEXT_BOX_LOCATOR = By.xpath("//input[@data-placeholder='Last Name']");
	private static final By CONTACT_TEXT_BOX_LOCATOR = By.xpath("//input[@data-placeholder='Contact No.']");
	private static final By EMAIL_TEXT_BOX_LOCATOR = By.xpath("//input[@data-placeholder='Email Id.']");
	private static final By FLAT_NUMBER_TEXT_BOX_LOCATOR = By.xpath("//input[@data-placeholder='Flat/Society No.']");
	private static final By APARTMENT_NAME_TEXT_BOX_LOCATOR = By.xpath("//input[@data-placeholder='Apartment Name']");
	private static final By LANDMARK_TEXT_BOX_LOCATOR = By.xpath("//input[@data-placeholder='Choose a Landmark']");
	private static final By STREET_NAME_TEXT_BOX_LOCATOR = By.xpath("//input[@data-placeholder='Street Name.']");
	private static final By AREA_NAME_TEXT_BOX_LOCATOR = By.xpath("//input[@data-placeholder='Area']");
	private static final By STATE_NAME_TEXT_BOX_LOCATOR = By.xpath("//input[@data-placeholder='Select State']");
	private static final By PIN_CODE_TEXT_BOX_LOCATOR = By.xpath("//input[@data-placeholder='Pincode']");
	private static final By SUBMIT_BUTTON_LOCATOR = By.xpath("//span[contains(text(),'Submit')]/..");
	private static final By JOB_TOAST_LOCATOR = By.xpath("	//span[contains(text(),\"Job created successfully\")]");

	public CreateJobPage(WebDriver wd) {
		super(wd);
		this.wd=wd;
		// TODO Auto-generated constructor stub
	}
	public String createjob() {
		SelectFromDropdown(OEM_LOCATOR," Apple ");
		SelectFromDropdown(PRODUCT_NAME_DROPDOWN_LOCATOR," IPhone ");
		sleepFor(2);
		SelectFromDropdown(MODEL_NAME_DROPDOWN_LOCATOR," Iphone 11 ");
		//System.out.println(po.getImeinumber());
		sleepFor(2);
		enterText(IMEI_TEXT_BOX_LOCATOR,"67433345875432");
		enterText(PURCHASE_DATE_BOX_LOCATOR,"12/12/2021");
		SelectFromDropdown( WARRANTY_DROPDOWN_LOCATOR," In Warrenty ");
		SelectFromDropdown( PROBLEM_DROPDOWN_LOCATOR," Overheating ");
		enterText(REMARK_TEXT_BOX_LOCATOR,"too much heat");
		enterText(FIRST_NAME_TEXT_BOX_LOCATOR,"nirma");
		
		enterText(LAST_NAME_TEXT_BOX_LOCATOR,"fida");
		enterText(CONTACT_TEXT_BOX_LOCATOR,"7654328978");
		enterText(EMAIL_TEXT_BOX_LOCATOR,"fida@gmail.com");
		enterText(FLAT_NUMBER_TEXT_BOX_LOCATOR,"78");
		enterText(APARTMENT_NAME_TEXT_BOX_LOCATOR,"nivas");
		enterText(LANDMARK_TEXT_BOX_LOCATOR,"near school");
		enterText(STREET_NAME_TEXT_BOX_LOCATOR,"sush street");
		enterText(AREA_NAME_TEXT_BOX_LOCATOR,"temp");
		enterText(STATE_NAME_TEXT_BOX_LOCATOR,"karnataka");
		enterText(PIN_CODE_TEXT_BOX_LOCATOR,"78654499");
		clickon(SUBMIT_BUTTON_LOCATOR);
		String job_id=getvisibletext(JOB_TOAST_LOCATOR);
		return job_id;
		
	}
//	public String createjob(CreateJobPOJO po) {
//		SelectFromDropdown(OEM_LOCATOR,po.getOemname());
//		SelectFromDropdown(PRODUCT_NAME_DROPDOWN_LOCATOR,po.getProductname());
//		sleepFor(2);
//		SelectFromDropdown(MODEL_NAME_DROPDOWN_LOCATOR,po.getModelname());
//		//System.out.println(po.getImeinumber());
//		sleepFor(2);
//		enterText(IMEI_TEXT_BOX_LOCATOR,po.getImeinumber());
//		enterText(PURCHASE_DATE_BOX_LOCATOR,po.getDateofpurchase());
//		SelectFromDropdown( WARRANTY_DROPDOWN_LOCATOR,po.getWarrentystatus());
//		SelectFromDropdown( PROBLEM_DROPDOWN_LOCATOR,po.getProblem());
//		enterText(REMARK_TEXT_BOX_LOCATOR,po.getRemarks());
//		enterText(FIRST_NAME_TEXT_BOX_LOCATOR,po.getFirstname());
//		
//		enterText(LAST_NAME_TEXT_BOX_LOCATOR,po.getLastname());
//		enterText(CONTACT_TEXT_BOX_LOCATOR,po.getContact());
//		enterText(EMAIL_TEXT_BOX_LOCATOR,po.getEmail());
//		enterText(FLAT_NUMBER_TEXT_BOX_LOCATOR,po.getFlatnum());
//		enterText(APARTMENT_NAME_TEXT_BOX_LOCATOR,po.getApartmentname());
//		enterText(LANDMARK_TEXT_BOX_LOCATOR,po.getLandmark());
//		enterText(STREET_NAME_TEXT_BOX_LOCATOR,po.getStreetname());
//		enterText(AREA_NAME_TEXT_BOX_LOCATOR,po.getArea());
//		enterText(STATE_NAME_TEXT_BOX_LOCATOR,po.getState());
//		enterText(PIN_CODE_TEXT_BOX_LOCATOR,po.getPincode());
//		clickon(SUBMIT_BUTTON_LOCATOR);
//		String job_id=getvisibletext(JOB_TOAST_LOCATOR);
//		return job_id;
//		
//	}
	public String createjob(CreateJobPOJO po) {
		SelectFromDropdown(OEM_LOCATOR," Apple ");
		SelectFromDropdown(PRODUCT_NAME_DROPDOWN_LOCATOR," IPhone ");
		sleepFor(2);
		SelectFromDropdown(MODEL_NAME_DROPDOWN_LOCATOR," Iphone 11 ");
		//System.out.println(po.getImeinumber());
		sleepFor(2);
		enterText(IMEI_TEXT_BOX_LOCATOR,po.getImeinumber());
		enterText(PURCHASE_DATE_BOX_LOCATOR,"12/12/2021");
		SelectFromDropdown( WARRANTY_DROPDOWN_LOCATOR," In Warrenty ");
		SelectFromDropdown( PROBLEM_DROPDOWN_LOCATOR," Overheating ");
		enterText(REMARK_TEXT_BOX_LOCATOR,"too much heat");
		enterText(FIRST_NAME_TEXT_BOX_LOCATOR,"nirma");
		
		enterText(LAST_NAME_TEXT_BOX_LOCATOR,"fida");
		enterText(CONTACT_TEXT_BOX_LOCATOR,"7654328978");
		enterText(EMAIL_TEXT_BOX_LOCATOR,"fida@gmail.com");
		enterText(FLAT_NUMBER_TEXT_BOX_LOCATOR,"78");
		enterText(APARTMENT_NAME_TEXT_BOX_LOCATOR,"nivas");
		enterText(LANDMARK_TEXT_BOX_LOCATOR,"near school");
		enterText(STREET_NAME_TEXT_BOX_LOCATOR,"sush street");
		enterText(AREA_NAME_TEXT_BOX_LOCATOR,"temp");
		enterText(STATE_NAME_TEXT_BOX_LOCATOR,"karnataka");
		enterText(PIN_CODE_TEXT_BOX_LOCATOR,"78654499");
		clickon(SUBMIT_BUTTON_LOCATOR);
		String job_id=getvisibletext(JOB_TOAST_LOCATOR);
		return job_id;
		
	}
	public String getJobCreated() {
		String job_id=getvisibletext(JOB_TOAST_LOCATOR);
		
		
		return job_id;
		
		
		
		
	}
	

}
