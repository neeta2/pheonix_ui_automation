package com.ui.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


import com.ui.pojo.SearchJobOrImeiPOJO;
import com.utility.BrowserUtil;

public class DashBoardPage extends BrowserUtil {
	private WebDriver wd;
	private static final By CREATE_JOB_LINK_LOCATOR = By.xpath("//span[contains(text(),\"Create Job\")]/../../..");
	private static final By  USERNAME_ICON_LOCATOR = By
			.xpath("//mat-icon[@data-mat-icon-name=\"user-circle\"]/../../..");
	private static final By USERNAME_TEXT_LOCATOR = By.xpath("//span[contains(text(),\"Signed in as\")]/../span[2]");
	private static final By CREATE_JOB_BUTTON_LOCATOR=By.xpath("//span[contains(text(),\"Create Job\")]/../../..");
	private static final By TABLE_LOCATOR=By.tagName("mat-table");
	private static final By TABLE_ROW_LOCATOR = By.xpath(".//mat-row");
	private static final By TABLE_CELL_LOCATOR = By.xpath(".//mat-cell");
	private static final By READY_FOR_DELIVERY_LINK_LOCATOR=By.xpath("//span[contains(text(),\" Ready for delivery \")]/../../..");
	private static final By DOWNLOAD_DELIVERYNOTE_LINK_LOCATOR=By.xpath("//span[contains(text(),\" Download Delivery Note \")]/../../..");
	private static final By INWARD_CC_JOBS_LINK_LOCATOR=By.xpath("//span[contains(text(),\"Inward CC Jobs\")]/../../..");
	private static final By SEARCH_FOR_JOB_IEMI_LOCATOR=By.xpath("//input[@placeholder=\"Search for a Job or IMEI\"]");
    private static final By SIGNOUT_LOCATOR=By.xpath("//span[contains(text(),\"Sign out\")]");
    private static final By JOB_IMEI_DETAILS_TABLE_LOCATOR=By.xpath("//mat-table[@class=\"mat-table cdk-table\"]");
	public DashBoardPage(WebDriver wd) {
		super(wd);
		this.wd=wd;
		// TODO Auto-generated constructor stub
	}
	public String getUserName() {
		clickon(USERNAME_ICON_LOCATOR);
		String username=getvisibletext(USERNAME_TEXT_LOCATOR);
		return username;
		
		
	}
	public CreateJobPage gotoCreateJobPage() {
		clickon(CREATE_JOB_LINK_LOCATOR);
		CreateJobPage createjob=new CreateJobPage(wd);
		return createjob;
		
	}
	public LoginPage signout() {
		clickon(USERNAME_ICON_LOCATOR);
		clickon(SIGNOUT_LOCATOR);
		LoginPage lpage=new LoginPage(wd);
		return lpage;
	}
	
	public List<SearchJobOrImeiPOJO> getJobOrImeiDetailsFromSearchJobOrImei(String jobORImei) {
		sleepFor(2);
		enterTextAndPressEnter(SEARCH_FOR_JOB_IEMI_LOCATOR,jobORImei);
		sleepFor(2);
		List<SearchJobOrImeiPOJO> jobOrimeiList=getListOfElements(TABLE_LOCATOR,TABLE_ROW_LOCATOR,TABLE_CELL_LOCATOR);
	    return jobOrimeiList;
	}
	
	
	
	

}
