package com.utility;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.github.javafaker.Faker;
import com.ui.pojo.CreateJobPOJO;

public class TestUtility {
	public static CreateJobPOJO CreateFakeDataForCreateJob() {
		Faker faker=new Faker();
	//com.ui.pojo.CreateJobPOJO createjobpojo=new com.ui.pojo.CreateJobPOJO("Apple","IPhone","Iphone 11","78654986432765","12/12/2022"," In Warrenty "," Overheating ","too much heating","neeta","ellur","5678654326","abc@gmail.com","22","hfddsfh","near busstop","medow","raichur","mp","45678");
		CreateJobPOJO pojo=new CreateJobPOJO(" Apple ", " IPhone ", " IPhone 11 ", faker.numerify("56############"), "12/12/2022", " In Warrenty ", " Overheating ", "too much", faker.name().toString(), faker.name().toString(), faker.numerify("98########"), faker.internet().emailAddress(), "89", "home", "near airport", "hub street", "vasavi nagar", "telangana", "679990");
		return pojo;
		
	}
	public static String getTime() {
		Date date = new Date();
		System.out.println(date.toString());

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY HH-mm");
		String formatedDate = sdf.format(date);

		return formatedDate;
	}

}	
