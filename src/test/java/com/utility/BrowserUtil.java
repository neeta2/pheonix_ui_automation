package com.utility;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ui.pojo.SearchJobOrImeiPOJO;

public abstract class BrowserUtil {
	private WebDriver wd;
	private static WebDriver screenshotwd;
	private WebDriverWait wait;
	public BrowserUtil(WebDriver wd) {
		super();
		this.wd = wd;
		wait = new WebDriverWait(wd,Duration.ofSeconds(50));
	}
	public void goToWebsite(String url) {
		wd.get(url);
	}
	public void viewinFullScreen() {
		wd.manage().window().maximize();
		
	}
	public void enterText(By locator,String texttoenter) {
		sleepFor(2);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		element.clear();
		element.sendKeys(texttoenter);
		
	}
	public void enterTextAndPressEnter(By locator,String text) {
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		element.sendKeys(text);
		element.sendKeys(Keys.ENTER);
		
	}
	public void clickon(By locator) {
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		element.click();
	}
	public String getvisibletext(By locator) {
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		String visibledata=element.getText();
		return visibledata;
		
	}
	public List<SearchJobOrImeiPOJO> getListOfElements(By tableLocator, By rowLocator, By cellLocator) {
		WebElement tableElement = wait.until(ExpectedConditions.visibilityOfElementLocated(tableLocator));
		WebElement rowElementTemp = wait.until(ExpectedConditions.visibilityOfElementLocated(rowLocator)); // *****
		List<WebElement> rowList = tableElement.findElements(rowLocator);
		System.out.println(rowList.size());
		Iterator<WebElement> rowIterator = rowList.iterator();
		WebElement rowElement;
		List<WebElement> cellList;
		List<SearchJobOrImeiPOJO> tableList = new ArrayList<SearchJobOrImeiPOJO>();
		while (rowIterator.hasNext()) {
			rowElement = rowIterator.next();
			cellList = rowElement.findElements(cellLocator);

			tableList.add(new SearchJobOrImeiPOJO(cellList.get(0).getText(), cellList.get(1).getText(),
					cellList.get(2).getText(), cellList.get(3).getText(), cellList.get(4).getText(),
					cellList.get(5).getText(), cellList.get(6).getText()));

		}
		return tableList;
	}
	public void sleepFor(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void SelectFromDropdown(By locator,String value) {
		sleepFor(4);
		clickon(locator);
		
		sleepFor(2);
		By oem_data_locator=By.xpath("//span[contains(text(),\"" + value + "\")]/..");
		clickon(oem_data_locator);
		
	}
	public static String takeScreenshot(String testname) {
		TakesScreenshot takescreenshot=(TakesScreenshot)screenshotwd;
		File screenshotdata=takescreenshot.getScreenshotAs(OutputType.FILE);
		File reportDirectory=new File(System.getProperty("user.dir")+"/screenshots");
		try {
			FileUtils.forceMkdir(reportDirectory);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String pathname = System.getProperty("user.dir") + "/screenshots//" + testname + ".png";
		File myFile = new File(pathname);
		try {
			myFile.createNewFile();
			FileUtils.copyFile(screenshotdata, myFile); // APACHE COMMON
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return pathname;
	}
	public String getPageTitile() {
		String title=wd.getTitle();
		return title;
	}
	

}
